<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 24/03/19
 * Time: 20:55
 */

function string2row($rowData)
{
    return explode(',', $rowData);
}

function row2string($row)
{
    $line = '';
    foreach ($row as $element) {
        if($line != '') {
            $line .= ',';
        }
        $line .= $element;
    }
    return $line;
}